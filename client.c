//
// Created by yakov and julia on 3/31/19.
//

#include <sys/wait.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include "client.h"
#include "core/communication.h"
#include "my_common.h"

bool acked = false;

static void send_stop_to_all(struct process *process);

static AllHistory *aggregate_history(struct process *process);

void wait_for_all_history(struct process *process);

bool is_all_history_received(struct process *process);

void client_loop(struct process *process) {
    wait_for_all_to_start(process);
    bank_robbery(process, (local_id) (process->process_count - 1));
    send_stop_to_all(process);
    wait_for_all_to_stop(process);
    wait_for_all_history(process);
    AllHistory *history = aggregate_history(process);
    print_history(history);
    free(history);

    int status = 0;
    while (wait(&status) > 0);
}

void send_stop_to_all(struct process *process) {
    notify_all(process, STOP);
}

void wait_for_all_history(struct process *process) {
    Message *message = malloc(sizeof(Message));

    while (!is_all_history_received(process)) {
        receive_any(process, message);
    }

    free(message);
}

bool is_all_history_received(struct process *process) {
    bool all_received = true;

    for (int id = 1; id < process->process_count; ++id) {
        if (bank_data(&process->processes[id])->history.s_id < 0) {
            all_received = false;
        }
    }

    return all_received;
}

AllHistory *aggregate_history(struct process *process) {
    AllHistory *history = malloc(sizeof(AllHistory));
    history->s_history_len = (uint8_t) (process->process_count - 1);

    for (int id = 1; id < process->process_count; ++id) {
        history->s_history[id - 1] = bank_data(&process->processes[id])->history;
    }

    return history;
}

void client_started(struct process *process, struct process_entry *from, MessageHeader *header) {
    bank_data(from)->process_state = P_STARTED;
    void *trash = malloc(header->s_payload_len);
    read(from->fd_to_read, trash, header->s_payload_len);
    free(trash);
}

void client_done(struct process *process, struct process_entry *from, MessageHeader *header) {
    bank_data(from)->process_state = P_DONE;
    void *trash = malloc(header->s_payload_len);
    read(from->fd_to_read, trash, header->s_payload_len);
    free(trash);
}

void client_ack(struct process *process, struct process_entry *from) {
    acked = true;
}

void client_balance_history(struct process *_, struct process_entry *from, MessageHeader *header) {
    BalanceHistory *balance = malloc(sizeof(BalanceHistory));

    if (read(from->fd_to_read, balance, header->s_payload_len) < 0) {
        perror("Unable to perform read call");
    }

    bank_data(from)->history = *balance;
    free(balance);
}

void transfer(void *parent_data, local_id src, local_id dst, balance_t amount) {
    Message *message = create_message(TRANSFER, parent_data);
    TransferOrder *transfer_order = malloc(sizeof(TransferOrder));
    transfer_order->s_src = src;
    transfer_order->s_dst = dst;
    transfer_order->s_amount = amount;
    memcpy(message->s_payload, transfer_order, sizeof(TransferOrder));

    send(parent_data, src, message);

    while (!acked) {
        receive(parent_data, dst, message);
    }

    acked = false;
    free(message);
}
