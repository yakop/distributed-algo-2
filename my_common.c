//
// Created by yakov and julia on 3/31/19.
//

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "my_common.h"
#include "core/communication.h"
#include "unistd.h"

FILE *log_file;

void wait_for_all_to_start(struct process *process) {
    wait_for_all_to(P_STARTED, process);
    log(log_received_all_started_fmt, process->id);
}

void wait_for_all_to_stop(struct process *process) {
    wait_for_all_to(P_DONE, process);
    log(log_received_all_done_fmt, process->id);
}

void wait_for_all_to(enum process_state desired_state, struct process *process) {
    bool ok = false;
    local_id next_to_receive;

    while (!ok) {
        if ((next_to_receive = find_first_not_in_right_state(process, desired_state)) > 0) {
            Message *message = malloc(sizeof(Message));
            receive(process, next_to_receive, message);
            free(message);
        } else {
            ok = true;
        }
    }
}

local_id find_first_not_in_right_state(struct process *process, enum process_state desired_state) {
    for (int id = 1; id < process->process_count; ++id) {
        if (id != process->id && bank_data(&process->processes[id])->process_state != desired_state) {
            return (local_id) id;
        }
    }

    return 0;
}


void notify_all(struct process *process, MessageType type) {
    Message *const message = create_message(type, process);
    send_multicast(process, message);
    free(message);
}

Message *create_message(MessageType type, struct process *process) {
    Message *msg = malloc(sizeof(Message));
    msg->s_header = *create_message_header(type);
    msg->s_header.s_local_time = get_physical_time();

    switch (type) {
        case STARTED:
            set_payload(
                    log_started_fmt,
                    process->id,
                    getpid(),
                    getppid(),
                    bank_data(process)->balance
            );
            msg->s_header.s_payload_len = (uint16_t) strlen(msg->s_payload);
            break;
        case DONE:
            set_payload(log_done_fmt, process->id, bank_data(process)->balance);
            msg->s_header.s_payload_len = (uint16_t) strlen(msg->s_payload);
            break;
        case BALANCE_HISTORY: {
            BalanceHistory history = bank_data(process)->history;
            memcpy(msg->s_payload, &history, sizeof(BalanceHistory));
            msg->s_header.s_payload_len = 2 * sizeof(local_id) + history.s_history_len * sizeof(BalanceState);
        }
            break;
        case TRANSFER:
            msg->s_header.s_payload_len = sizeof(TransferOrder);
            break;
        case ACK:
            msg->s_header.s_payload_len = 0;
            break;
        case STOP:
            msg->s_header.s_payload_len = 0;
        default:
            break;
    }

    return msg;
}

MessageHeader *create_message_header(MessageType type) {
    MessageHeader *msg_header = malloc(sizeof(MessageHeader));
    msg_header->s_magic = MESSAGE_MAGIC;
    msg_header->s_type = type;

    return msg_header;
}
