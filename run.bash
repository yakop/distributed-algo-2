#!/usr/bin/env bash

export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/path/to/pa2/dir"
export LD_PRELOAD=/home/yakov/CLionProjects/distributed_algo_2/pa2345_starter_code/lib64/libruntime.so
clang -std=c99 -Wall -pedantic *.c core/* pa2345_starter_code/* -Lpa2345_starter_code/lib64/ -lruntime

./a.out -p 5