//
// Created by yakov and julia on 3/31/19.
//

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "bank.h"
#include "my_common.h"
#include "core/communication.h"
#include "pa2345_starter_code/banking.h"

static void send_history(struct process *process);

static void send_started_to_all(struct process *process);

static void do_main_loop(struct process *process);

void update_balance(const struct process *process, balance_t amount);

void redirect_transfer(struct process *process, const TransferOrder *transfer_order);

void fill_history(const struct process *process, balance_t upper_timestamp);

void bank_loop(struct process *process) {
    send_started_to_all(process);
    wait_for_all_to_start(process);
    do_main_loop(process);
    send_history(process);

    exit(0);
}

void send_started_to_all(struct process *process) {
    notify_all(process, STARTED);
}

void do_main_loop(struct process *process) {
    Message *message = malloc(sizeof(Message));

    while (bank_data(process)->process_state != P_STOPPED) {
        receive_any(process, message);
    }

    free(message);
    notify_all(process, DONE);
    wait_for_all_to_stop(process);
}

void send_history(struct process *process) {
    fill_history(process, (balance_t) (get_physical_time() + 1));
    Message *message = create_message(BALANCE_HISTORY, process);
    send(process, PARENT_ID, message);
}

void fill_history(const struct process *process, balance_t upper_timestamp) {
    balance_t previous_balance =
            bank_data(process)->history.s_history[bank_data(process)->history.s_history_len - 1].s_balance;
    const timestamp_t previous_ts =
            bank_data(process)->history.s_history[bank_data(process)->history.s_history_len - 1].s_time;
    timestamp_t ts = previous_ts;

    while (++ts < upper_timestamp) {
        bank_data(process)->history.s_history[bank_data(process)->history.s_history_len].s_balance = previous_balance;
        bank_data(process)->history.s_history[bank_data(process)->history.s_history_len].s_time = ts;
        bank_data(process)->history.s_history_len++;
    }
}

void bank_started(struct process *process, struct process_entry *from, MessageHeader *header) {
    bank_data(from)->process_state = P_STARTED;
    void *trash = malloc(header->s_payload_len);
    read(from->fd_to_read, trash, header->s_payload_len);
    free(trash);
}

void bank_stop(struct process *process, struct process_entry *from) {
    bank_data(process)->process_state = P_STOPPED;
}

void bank_transfer(struct process *process, struct process_entry *from) {
    TransferOrder *transfer_order = malloc(sizeof(TransferOrder));
    read(from->fd_to_read, transfer_order, sizeof(TransferOrder));

    if (from->id == PARENT_ID) {
        redirect_transfer(process, transfer_order);
        update_balance(process, -transfer_order->s_amount);
    } else {
        log(log_transfer_in_fmt, process->id, transfer_order->s_amount, transfer_order->s_src);
        send(process, PARENT_ID, create_message(ACK, process));
        update_balance(process, transfer_order->s_amount);
    }

    free(transfer_order);
}

void redirect_transfer(struct process *process, const TransferOrder *transfer_order) {
    Message *message = create_message(TRANSFER, process);
    memcpy(message->s_payload, transfer_order, sizeof(TransferOrder));
    send(process, transfer_order->s_dst, message);
    log(log_transfer_out_fmt, process->id, transfer_order->s_amount, transfer_order->s_dst);
    free(message);
}

void update_balance(const struct process *process, balance_t amount) {
    bank_data(process)->balance += amount;
    fill_history(process, get_physical_time());
    bank_data(process)->history.s_history[bank_data(process)->history.s_history_len].s_time = get_physical_time();
    bank_data(process)->history.s_history[bank_data(process)->history.s_history_len].s_balance =
            bank_data(process)->balance;
    bank_data(process)->history.s_history_len++;
}

void bank_done(struct process *process, struct process_entry *from, MessageHeader *header) {
    bank_data(from)->process_state = P_DONE;
    void *trash = malloc(header->s_payload_len);
    read(from->fd_to_read, trash, header->s_payload_len);
    free(trash);
}
