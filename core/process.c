//
// Created by yakov and julia on 3/9/19.
//

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include "process.h"
#include "../pa2345_starter_code/ipc.h"
#include "../pa2345_starter_code/pa2345.h"
#include "../pa2345_starter_code/banking.h"

struct process *new_process(int process_id, size_t process_count, void (*run_function)(), void (*operations[])()) {
    struct process *process = malloc(sizeof(struct process));
    process->id = process_id;
    process->process_count = process_count;
    process->run_function = run_function;
    process->receive_operations[STARTED] = operations[STARTED];
    process->receive_operations[DONE] = operations[DONE];
    process->receive_operations[ACK] = operations[ACK];
    process->receive_operations[STOP] = operations[STOP];
    process->receive_operations[TRANSFER] = operations[TRANSFER];
    process->receive_operations[BALANCE_HISTORY] = operations[BALANCE_HISTORY];
    process->receive_operations[CS_REQUEST] = operations[CS_REQUEST];
    process->receive_operations[CS_REPLY] = operations[CS_REPLY];
    process->receive_operations[CS_RELEASE] = operations[CS_RELEASE];

    return process;
}
