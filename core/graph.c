//
// Created by yakov and julia on 3/31/19.
//

#include "graph.h"
#include "../pa2345_starter_code/ipc.h"
#include <unistd.h>
#include <fcntl.h>

struct edge *edges[MAX_PROCESS_ID + 1][MAX_PROCESS_ID + 1];

void create_edge(int from, int to);

void build_fd_graph(size_t process_count) {
    for (int i = 0; i < process_count; ++i) {
        for (int j = 0; j < process_count; ++j) {
            if (i != j) {
                create_edge(i, j);
            }
        }
    }
}

void create_edge(int from, int to) {
    int fds[2];
    pipe(fds);
    fcntl(fds[0], F_SETFL, O_NONBLOCK);
    fcntl(fds[1], F_SETFL, O_NONBLOCK);
    edges[from][to] = malloc(sizeof(struct edge));
    edges[from][to]->fd_to_write = fds[1];
    edges[from][to]->fd_to_read = fds[0];
}

void close_external_fds(int process_id, size_t process_count) {
    for (int i = 0; i < process_count; ++i) {
        for (int j = 0; j < process_count; ++j) {
            if (i != j && j != process_id) {
                close(edges[i][j]->fd_to_write);
            }

            if (i != j && i != process_id) {
                close(edges[i][j]->fd_to_read);
            }
        }
    }
}
