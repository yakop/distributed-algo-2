//
// Created by yakov and julia on 3/31/19.
//

#include "communication.h"
#include "process.h"
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

int send(void *self, local_id dst, const Message *msg) {
    const struct process *process = (const struct process *) self;

    int fd_to_send = process->processes[dst].fd_to_write;

    if (write(fd_to_send, msg, sizeof(MessageHeader) + msg->s_header.s_payload_len) < 0) {
        return 1;
    }

    return 0;
}

int send_multicast(void *self, const Message *msg) {
    const struct process *process = (const struct process *) self;
    int status = 0;

    for (int8_t i = 0; i < process->process_count; ++i) {
        if (i != process->id) {
            status |= send(self, i, msg);
        }
    }

    return status;
}

int receive(void *self, local_id from, Message *msg) {
    struct process *this_process = (struct process *) self;
    struct process_entry *process_to_receive = &this_process->processes[from];

    if (read(process_to_receive->fd_to_read, &msg->s_header, sizeof(MessageHeader)) <= 0) {
        return 1;
    }

    switch (msg->s_header.s_type) {
        case STARTED:
            receive_started(this_process, process_to_receive);
            break;
        case DONE:
            receive_done(this_process, process_to_receive);
            break;
        case ACK:
            receive_ack(this_process, process_to_receive);
            break;
        case STOP:
            receive_stop(this_process, process_to_receive);
            break;
        case TRANSFER:
            receive_transfer(this_process, process_to_receive);
            break;
        case BALANCE_HISTORY:
            receive_balance_history(this_process, process_to_receive);
            break;
        case CS_REQUEST:
            receive_cs_request(this_process, process_to_receive);
            break;
        case CS_REPLY:
            receive_cs_reply(this_process, process_to_receive);
            break;
        case CS_RELEASE:
            receive_cs_release(this_process, process_to_receive);
            break;
        default:
            break;
    }

    return 0;
}

int receive_any(void *self, Message *msg) {
    struct process *this_process = (struct process *) self;

    for (int id = 0; id < this_process->process_count; ++id) {
        if (id != this_process->id) {
            if (receive(self, (local_id) id, msg) == 0) {
                return 0;
            }
        }
    }

    return 1;
}
