//
// Created by yakov and julia on 3/31/19.
//

#ifndef DISTRIBUTED_ALGO_2_IO_H
#define DISTRIBUTED_ALGO_2_IO_H

#define log(template, ...) fprintf(log_file, template, get_physical_time(), __VA_ARGS__)

#include "stdio.h"
#include "../pa2345_starter_code/ipc.h"
#include "../pa2345_starter_code/banking.h"

extern FILE *log_file;

int send(void *self, local_id dst, const Message *msg);

int send_multicast(void *self, const Message *msg);

int receive(void *self, local_id from, Message *msg);

#endif //DISTRIBUTED_ALGO_2_IO_H
