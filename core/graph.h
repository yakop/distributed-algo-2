//
// Created by yakov and julia on 3/31/19.
//

#ifndef DISTRIBUTED_ALGO_2_GRAPH_H
#define DISTRIBUTED_ALGO_2_GRAPH_H

#include <stdlib.h>

struct edge {
    int fd_to_write;
    int fd_to_read;
};

void build_fd_graph(size_t process_count);

void close_external_fds(int process_id, size_t process_count);

#endif //DISTRIBUTED_ALGO_2_GRAPH_H
