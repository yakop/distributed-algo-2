//
// Created by yakov and julia on 3/9/19.
//

#ifndef DISTRIBUTED_ALGO_1_PROCESS_H
#define DISTRIBUTED_ALGO_1_PROCESS_H

#include "../pa2345_starter_code/ipc.h"
#include "../pa2345_starter_code/pa2345.h"
#include <stdbool.h>

#define OPERATION_COUNT 9

#define run(process) process->run_function(process)
#define receive_started(process, ...) (process->receive_operations[STARTED])(process, __VA_ARGS__, &msg->s_header)
#define receive_done(process, ...) (process->receive_operations[DONE])(process, __VA_ARGS__, &msg->s_header)
#define receive_ack(process, ...) (process->receive_operations[ACK])(process, __VA_ARGS__, &msg->s_header)
#define receive_stop(process, ...) (process->receive_operations[STOP])(process, __VA_ARGS__, &msg->s_header)
#define receive_transfer(process, ...) (process->receive_operations[TRANSFER])(process, __VA_ARGS__, &msg->s_header)
#define receive_balance_history(process, ...) (process->receive_operations[BALANCE_HISTORY])(process, __VA_ARGS__, &msg->s_header)
#define receive_cs_request(process, ...) (process->receive_operations[CS_REQUEST])(process, __VA_ARGS__, &msg->s_header)
#define receive_cs_reply(process, ...) (process->receive_operations[CS_REPLY])(process, __VA_ARGS__, &msg->s_header)
#define receive_cs_release(process, ...) (process->receive_operations[CS_RELEASE])(process, __VA_ARGS__, &msg->s_header)

struct process_entry {
    int id;
    void* data;
    int fd_to_read;
    int fd_to_write;
};

struct process {
    int id;
    void* data;
    size_t process_count;
    struct process_entry processes[MAX_PROCESS_ID + 1];
    void (*run_function)();
    void (*receive_operations[OPERATION_COUNT])();
};

struct process *new_process(int process_id, size_t process_count, void (*run_function)(), void (*operations[])());

#endif //DISTRIBUTED_ALGO_1_PROCESS_H
