//
// Created by yakov and julia on 3/31/19.
//

#ifndef DISTRIBUTED_ALGO_2_BANK_H
#define DISTRIBUTED_ALGO_2_BANK_H

#include "core/process.h"

void bank_loop(struct process *process);

void bank_started(struct process *process, struct process_entry *from, MessageHeader *header);

void bank_done(struct process *process, struct process_entry *from, MessageHeader *header);

void bank_stop(struct process *process, struct process_entry *from);

void bank_transfer(struct process *process, struct process_entry *from);


#endif //DISTRIBUTED_ALGO_2_BANK_H
