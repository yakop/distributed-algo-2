//
// Created by yakov and julia on 3/31/19.
//

#ifndef DISTRIBUTED_ALGO_2_COMMON_H
#define DISTRIBUTED_ALGO_2_COMMON_H

#include "pa2345_starter_code/ipc.h"
#include "pa2345_starter_code/banking.h"
#include "core/process.h"

#define set_payload(template, ...) sprintf(msg->s_payload, template, get_physical_time(), __VA_ARGS__)
#define bank_data(bank) ((struct bank_data *) ((bank)->data))
#define process_data(bank) ((struct bank_data *) ((bank)->data))

enum process_state {
    P_NEW,
    P_STARTED,
    P_STOPPED,
    P_DONE
};

struct bank_data {
    enum process_state process_state;
    balance_t balance;
    BalanceHistory history;
};

void wait_for_all_to_start(struct process *process);

void wait_for_all_to_stop(struct process *process);

void wait_for_all_to(enum process_state desired_state, struct process *process);

local_id find_first_not_in_right_state(struct process *process, enum process_state desired_state);

void notify_all(struct process *process, MessageType type);

Message *create_message(MessageType type, struct process *process);

MessageHeader *create_message_header(MessageType type);

#endif //DISTRIBUTED_ALGO_2_COMMON_H
