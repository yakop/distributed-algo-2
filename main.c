#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include "core/process.h"
#include "pa2345_starter_code/ipc.h"
#include "pa2345_starter_code/common.h"
#include "core/graph.h"
#include "my_common.h"
#include "client.h"
#include "bank.h"

extern struct edge *edges[MAX_PROCESS_ID + 1][MAX_PROCESS_ID + 1];

extern FILE *log_file;

static size_t process_count;

static balance_t balances[MAX_PROCESS_ID];

static void prepare_log_file();

static void run_bank_process(int id);

static void run_client_process();

static struct process *prepare_process(int process_id, void (*run_function)(), void (*operations[])());

static void parse_balances(char *const *argv);

int main(int argc, char **argv) {
    prepare_log_file();
    process_count = (size_t) atoi(argv[2]) + 1;

    parse_balances(argv);
    build_fd_graph(process_count);

    for (int id = 1; id < process_count; ++id) {
        run_bank_process(id);
    }

    run_client_process();
    return 0;
}

void parse_balances(char *const *argv) {
    for (int i = 1; i < process_count; ++i) {
        balances[i] = (balance_t) atoi(argv[i + 2]);
    }
}

void prepare_log_file() {
    log_file = fopen(events_log, "w");
}

void run_bank_process(int id) {
    int pid = fork();

    if (pid == 0) {
        void (*operations[])() = {
                bank_started,
                bank_done,
                NULL,
                bank_stop,
                bank_transfer
        };

        struct process *process = prepare_process(id, bank_loop, operations);
        process->data = malloc(sizeof(struct bank_data));
        bank_data(process)->balance = balances[id];
        bank_data(process)->history.s_id = (local_id) process->id;
        bank_data(process)->history.s_history_len = 1;
        bank_data(process)->history.s_history[0].s_time = get_physical_time();
        bank_data(process)->history.s_history[0].s_balance = bank_data(process)->balance;
        bank_data(process)->history.s_history[0].s_balance_pending_in = 0;
        run(process);
    }
}

void run_client_process() {
    void (*operations[])() = {
            client_started,
            client_done,
            client_ack,
            NULL,
            NULL,
            client_balance_history,
    };

    struct process *process = prepare_process(PARENT_ID, client_loop, operations);
    run(process);
}

struct process *prepare_process(int process_id, void (*run_function)(), void (*operations[])()) {
    struct process *process = new_process(process_id, process_count, run_function, operations);

    for (int id = 0; id < process_count; ++id) {
        if (id != process_id) {
            struct edge *edge_for_current_to_read = edges[process_id][id];
            struct edge *edge_for_current_to_write = edges[id][process_id];
            process->processes[id].id = id;
            process->processes[id].data = malloc(sizeof(struct bank_data));
            process->processes[id].fd_to_write = edge_for_current_to_write->fd_to_write;
            process->processes[id].fd_to_read = edge_for_current_to_read->fd_to_read;
            bank_data(&process->processes[id])->process_state = P_NEW;
            bank_data(&process->processes[id])->history.s_id = -1;
        }
    }

    close_external_fds(process_id, process_count);
    return process;
}
