//
// Created by yakov and julia on 3/31/19.
//

#ifndef DISTRIBUTED_ALGO_2_CLIENT_H
#define DISTRIBUTED_ALGO_2_CLIENT_H

#include "core/process.h"
#include "pa2345_starter_code/banking.h"

void client_loop(struct process *process);

void client_started(struct process *process, struct process_entry *from, MessageHeader *header);

void client_done(struct process *process, struct process_entry *from, MessageHeader *header);

void client_ack(struct process *process, struct process_entry *from);

void client_balance_history(struct process *process, struct process_entry *from, MessageHeader *header);

#endif //DISTRIBUTED_ALGO_2_CLIENT_H
